#include <iostream>
#include "functions.hpp"

namespace saw {
	double f(double x) {
		return(pow(x, 2) - 10);
	}

	double Bisection(double a, double b, double eps) {
		while (b - a > eps)
		{
			double x = (b + a) / 2;
			if ((abs(x - a) < eps) || (abs(x - b) < eps))
				return x;
			if (f(a) * f(x) == 0)
				break;
			else if (f(a) * f(x) > 0)
				a = x;
			else
				b = x;
		}

	}
}