#pragma once

namespace saw {
	double f(double x);
	double Bisection(double a, double b, double eps);
}