#include <iostream>
#include "functions.hpp"
#include<cmath>

using namespace saw;

int main() {
	double eps= 1e-4;
	std::cout<<Bisection(0, DBL_MAX, eps);
}