#include "functions.hpp" 
#include <iostream>
#include <chrono>

namespace saw {
    void QuickSort(int mas[N], int a, int b)
    {
        if (a >= b)
            return;
        int m = (rand() * rand()) % (b - a + 1) + a;
        int k = mas[m];
        int l = a - 1;
        int r = b + 1;
        while (1)
        {
            do l = l + 1; while (mas[l] < k);
            do r = r - 1; while (mas[r] > k);
            if (l >= r)
                break;
            std::swap(mas[l], mas[r]);
        }
        r = l;
        l = l - 1;
        QuickSort(mas, a, l);
        QuickSort(mas, r, b);
    }


    int Search(int a, int b, int k, int mas[N]) {
        if (k < mas[a])
            return -1;
        if (k == mas[a])
            return a;
        if (k > mas[b])
            return -1;
        a = 0;
        b = N - 1;
        while (a + 1 < b) {
            int c = int((a + b) / 2);
            if (k > mas[c])
                a = c;
            else
                b = c;
        }
        if (mas[b] == k)
            return b;
        else if (mas[a] == k)
            return a;
        else
            return -1;
    }

    Timer::Timer() : m_beg(clock_t::now()) {}
    void Timer::reset() {
        m_beg = clock_t::now();
    }
    double Timer::elapsed() const {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }


}