#pragma once
#define N 100000
#include <chrono>
#include <iostream>

namespace saw {
	void QuickSort(int mas[N], int a, int b);
	
    int Search(int a, int b, int k, int mas[N]);
    
    class Timer {
    private:
        using clock_t = std::chrono::high_resolution_clock;
        using second_t = std::chrono::duration<double, std::ratio<1> >;
        std::chrono::time_point<clock_t> m_beg;
    public:
        Timer();
        void reset();
        double elapsed() const;
    };
}